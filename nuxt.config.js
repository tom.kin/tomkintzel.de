export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Tom Kintzel - Startseite',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { 
        hid: 'description',
        name: 'description',
        content: 'Du suchst einen Entwickler für deine Webseite? Kontaktiere mich - Tom Kintzel'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;900&display=swap'
      }
    ],
    script: [
      {
        src: "https://kit.fontawesome.com/13b20a1162.js",
        crossorigin: "anonymous"
      }
    ]
  },

  sitemap: {
    hostname: 'https://tomkintzel.de',
    gzip: true,
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/assets/css/main.css'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/sitemap'
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },
  target: 'static',
  env: {
    privateToken: process.env.GITLAB_PERSONAL_TOKEN
  }
}
